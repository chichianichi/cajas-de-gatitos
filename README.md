# Cajas de Gatitos
<p>Proyecto de práctica para el desarrollo Frontend de páginas estáticas sin frameworks. <br>
Simula una página informativa de una empresa que produce cajas para gatitos, cuenta con las siguientes secciones:
</p>
<ul>
    <li>¿Quiénes Somos?: Cuenta con información histórica de la empresa.</li>
    <li>Nuestros productos: Muestra los productos disponibles y una breve descripción de ellos.</li>
    <li>Contáctanos: Sección con de contacto.</li>    
</ul>

<h2>El proyecto cuenta con las siguientes características y funcionalidades:</h2>

<ul>
    <li>Desarrollado con estructura HTML.</li>
    <li>Estilos implementados con CSS.</li>
    <li>Alertas personalizadas con [SweetAlert](https://sweetalert.js.org/).</li> 
    <li>Botón Paypal funcional.</li>   
</ul>

<h5>
    Ahora es posible visualizar la página en ejecución, previamente se ha configurado un website estático con la ayuda de la herramienta GitLab Pages,
    en conjunto  con   GitLab CI y GitLab Runner.
</h5>

### Watch Online
##### Si te gustaría probar la página en ejecución has click [Aquí](https://chichianichi.gitlab.io/cajas-de-gatitos "Aquí")

